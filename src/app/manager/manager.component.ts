import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { MatDialog } from '@angular/material/dialog';
import { DialogBoxComponent} from '../dialog-box/dialog-box.component'
import { jsPDF } from "jspdf";

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.css']
})
export class ManagerComponent {
  
  show:any;
  constructor(private http:HttpClient,public matDialog:MatDialog){
  }
  // Post
  post(PostData:{firstname:string;lastname:string}){
    this.http.post('https://management-f1e19-default-rtdb.firebaseio.com/data.json',PostData).subscribe((Response)=>{
      console.table(Response);
    });
  }
  // Fetch
  onShow(){
    this.http.get('https://management-f1e19-default-rtdb.firebaseio.com/data.json/').subscribe((data:any)=>{
        console.table(data);
        this.show=data;
      });  
  }
  hide=false
  role:any;
  onget(){
    this.http.get('https://management-f1e19-default-rtdb.firebaseio.com/role/role.json').subscribe((data:any)=>{
        console.table(data);
        this.role=data;
      });  
      this.hide=true
  }
  
  // Delete
  onDelete(){this.http.delete('https://management-f1e19-default-rtdb.firebaseio.com/data.json').subscribe((Response)=>{
    console.log(Response);
    });

  }
  // Clear
     //  Date to Age Convert
     date=Date()
     curDate:number=2023
     result:any
     convert(){
       this.date= this.date.slice(0,4)
       this.result=this.curDate-Number(this.date)
       console.log(this.result);
      }
      //  op
      add=false
      openDialog(){
      this.matDialog.open(DialogBoxComponent);
      this.add=true
    }
    // To make Pdf
    // data=""
    // makePdf(){
    //   this.data=this.show
    //   let pdf = new jsPDF()
    //   pdf.text("Hello",10,10)
    //   pdf.
    //   // pdf.text(this.data,50,100)
    //   pdf.save();
    // }
    // 
    // finalArList=''
    // makePdf(){
    //   const col=['firstname','lastname']
    //   const rows=[]
    //   const itemsNew = this.finalArList;
    //   itemsNew.forEach(Element=>{
    //     const temp = [Element.firstname,Element.lastname];
    //     rows.push(temp);
    //   });
    //   document.(col,rows[])
    // }
}


