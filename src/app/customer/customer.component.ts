import { Component } from '@angular/core';
import { DialogBoxComponent} from '../dialog-box/dialog-box.component'
import { HttpClient } from '@angular/common/http'
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent {
  show:any
  constructor(private http:HttpClient,public matDialog:MatDialog){}
  // Post
  post(PostData:{firstname:string;lastname:string}){
    this.http.post('https://customer001-1cd28-default-rtdb.firebaseio.com/data.json',PostData).subscribe((Response)=>{
      console.table(Response);
    });
  }
  // Fetch
  onShow(){
    this.http.get('https://customer001-1cd28-default-rtdb.firebaseio.com/data.json').subscribe((data:any)=>{
        console.table(data);
        this.show=data;
      });  
  }
  onDelete(){this.http.delete('https://customer001-1cd28-default-rtdb.firebaseio.com/data.json').subscribe((Response)=>{
    console.log(Response);
    });
  }
     //  Date to Age Convert
     date=Date()
     curDate:number=2023
     result:any
     convert(){
       this.date= this.date.slice(0,4)
       this.result=this.curDate-Number(this.date)
       console.log(this.result);
      }
      //  op
      openDialog(){
      this.matDialog.open(DialogBoxComponent);
    }
}
