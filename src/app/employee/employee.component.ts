import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { DialogBoxComponent} from '../dialog-box/dialog-box.component'


@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent {
  
  // Convert slider value to years
  formatLabel(value: number): string {
    if (value >= 1) {
      return Math.round(value / 1) + 'years';
    }
    return `${value}`;
  } 
  // Date to Age Convert
  date=Date()
  curDate:number=2023
  result:any
  convert(){
    this.date= this.date.slice(0,4)
    this.result=this.curDate-Number(this.date)
    console.log(this.result);
  }
  
  // Post to firebase
  constructor(private http:HttpClient){}
  post(PostData:{firstname:string;lastname:string}){
    this.http.post('https://employee001-bd48f-default-rtdb.firebaseio.com/data.json',PostData).subscribe((Response)=>{
      console.table(Response);
    });
   }
  // Fetch
  show:any
  onShow(){
    this.http.get('https://employee001-bd48f-default-rtdb.firebaseio.com/data.json').subscribe((data:any)=>{
        console.table(data);
        this.show=data;
      });  
  }
  onDelete(){this.http.delete('https://employee001-bd48f-default-rtdb.firebaseio.com/data.json').subscribe((Response)=>{
    console.log(Response);
    });
  }
  // popup
  onPopup(){
    prompt("Are you want to save")
  }
}